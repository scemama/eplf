include version
include make.config


all: bin/eplf

EZFIO.tar.gz:
	ln -s EZFIO.*.tar.gz EZFIO.tar.gz

EZFIO/config/eplf.config: EZFIO.tar.gz
	tar -zxf EZFIO.tar.gz
	cd EZFIO 
	if [ -e $@ ] ; then rm $@ ; fi
	ln -s $$PWD/eplf.config $@
	touch EZFIO/config/eplf.config

EZFIO/lib/libezfio.so: EZFIO/config/eplf.config
	$(MAKE) -C EZFIO/
	cp EZFIO/Python/ezfio.py scripts/

bin/ezfio.py: EZFIO/lib/libezfio.so
	if [ -e $@ ] ; then rm $@ ; fi
	ln -s $$PWD/EZFIO/Python/ezfio.py $@

bin/eplf: EZFIO/lib/libezfio.so
	make -C src

clean:
	- rm -rf EZFIO
	- rm bin/eplf
	- rm bin/ezfio.py
	- make -C src veryclean


archive:
	git archive --format=tar HEAD | gzip > eplf.$(VERSION).tar.gz
	cp bin/to_ezfio.exe to_ezfio.$(VERSION).exe

