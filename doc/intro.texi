@section QMC Formulation

The Electron Pair Localization Function (EPLF) is a three-dimensional function 
measuring locally the electron pairing in a molecular
system.@footnote{A. Scemama, P. Chaquin and M. Caffarel, @i{J. Chem. Phys.}
@b{121}, 1725-1735 (2004).}
An electron @math{i} located at @math{\vec{r}_i} is said to be paired to an electron @math{j}
located at @math{\vec{r}_j} if electron @math{j} is the closest electron to @math{i}. The
amount of electron pairing at point @math{{\vec r}} is therefore proportional to the
inverse of
@tex
$$
 d({\vec r}) = \sum_{i=1,N} \langle \Psi | \delta(\vec{r}-\vec{r}_i) \min_{j\ne i} r_{ij} | \Psi \rangle
$$
@end tex

where
@math{d({\vec r})} is the shortest electron-electron distance at @math{{\vec r}},
@math{\Psi({\vec r}_1,\dots,{\vec r}_N)} is the @math{N}-electron wave function
and
@math{r_{ij} = |{\vec r}_j - {\vec r}_i|}.

There are two different types of electron pairs: pairs of electrons with
the same spin @math{\sigma}, and pairs of electrons with opposite spins @math{\sigma}
and @math{\bar \sigma}.
Hence, two quantities are introduced:
@tex
$$
 d_{\sigma \sigma}({\vec r})  =  \sum_{i=1,N} \langle \Psi | \delta(\vec{r}-\vec{r}_i) \min_{j\ne i ; \sigma_i = \sigma_j} r_{ij} | \Psi \rangle 
$$
$$
 d_{\sigma {\bar \sigma}}({\vec r})  =  \sum_{i=1,N} \langle \Psi | \delta(\vec{r}-\vec{r}_i) \min_{j ; \sigma_i \ne \sigma_j} r_{ij} | \Psi \rangle 
$$
@end tex

The electron pair localization function is bounded in the @math{[-1,1]} interval,
and is defined as
@tex
$$
 {\rm EPLF}(\vec{r}) = 
  { d_{\sigma \sigma} (\vec{r})
        - d_{\sigma {\bar \sigma}} (\vec{r}) \over
         d_{\sigma \sigma} (\vec{r})
        + d_{\sigma {\bar \sigma}} (\vec{r}) }
$$
@end tex
When the pairing of spin-unlike electrons is predominant, @math{d_{\sigma \sigma}
> d_{\sigma {\bar \sigma}}} and @math{{\rm EPLF}({\vec r}) > 0}.
When the pairing of spin-like electrons is predominant, @math{d_{\sigma \sigma}
< d_{\sigma {\bar \sigma}}} and @math{{\rm EPLF}({\vec r}) < 0}.
When the electron pairing of spin-like and spin-unlike electrons is equivalent,
@math{{\rm EPLF}({\vec r}) \sim 0}.

This localization function does not depend on the type of wave function, as
opposed to the Electron Localization Function@footnote{A. D. Becke and K. E. Edgecombe
@i{J. Chem. Phys.} @b{92}, 5397 (1990).} (ELF) where the
wave function has to be expressed on a single-electron basis set.
The EPLF can therefore measure electron pairing using any kind of wave
function: Hartree-Fock (HF), configuration interaction (CI),
multi-configurational self consistent field (MCSCF) as well as Slater-Jastrow,
Diffusion Monte Carlo (DMC), Hylleraas wave functions, @i{etc}.
Due to the presence of the @math{\min} function in the definition of @math{d_{\sigma
\sigma}} and @math{d_{\sigma {\bar \sigma}}} these quantities cannot be evaluated
analytically, so quantum Monte Carlo (QMC) methods have been
used to compute three-dimensional EPLF grids via a statistical sampling of
@math{|\Psi({\vec r}_1,\dots,{\vec r}_N)|^2}.

@section Analytical Formulation

Recently, we proposed an alternative definition of the EPLF which is
analytically computable.@footnote{F. Alary, J.-L. Heully, A. Scemama, B. Garreau-de-Bonneval, K. I. Chane-Ching and M. Caffarel
@i{Theor. Chem. Acc.} @b{126}, 243 (2010).}
The @math{\min} function is approximated
using:
@tex
$$
\min_{j\neq i} r_{ij} = \lim_{\gamma \rightarrow \infty}
\sqrt{ -{1\over\gamma} \ln f(\gamma;r_{ij})} 
$$
$$
 f(\gamma;r_{ij}) =
\sum_{j \neq i} e^{ -\gamma r_{ij}^2 }
$$
@end tex

As @math{f(\gamma ; r_{ij})} has small
fluctuations in the regions of interest (bonding regions, lone pairs,@dots),
the integrals
@tex
$$
\left\langle \Psi \left| \delta({\vec r}-{\vec r}_i) \left( \lim_{\gamma \rightarrow \infty} \sqrt{ -{1 \over \gamma} \ln f(\gamma;r_{ij})} \right) \right| \Psi \right\rangle
$$
@end tex
can be approximated as
@tex
$$
\lim_{\gamma \rightarrow \infty} \sqrt{ -{1 \over \gamma} \ln 
\langle \Psi | \delta({\vec r}-{\vec r}_i) f(\gamma;r_{ij}) | \Psi \rangle }
$$
@end tex
The expectation values of the minimum distances are now given by:
@tex
$$
d_{\sigma \sigma}(\vec{r})  \sim 
\lim_{\gamma \rightarrow \infty} \sqrt{-{1 \over \gamma} \ln
f_{\sigma \sigma}(\gamma;\vec{r})} 
$$
$$
d_{\sigma \bar{\sigma}} (\vec{r})  \sim 
\lim_{\gamma \rightarrow \infty} \sqrt{-{1 \over \gamma} \ln
f_{\sigma \bar{\sigma}} (\gamma;\vec{r})}
$$
@end tex
with the two-electron integrals:
@tex
$$
f_{\sigma \sigma}(\gamma;\vec{r}) = 
\left \langle \Psi \left| \sum_{i=1}^{N} \delta(\vec{r}-\vec{r}_i)
\sum_{j \neq i ; \sigma_i = \sigma_j}^{N} 
e^{ -\gamma |\vec{r}_i - \vec{r}_j|^2 } \right| \Psi \right\rangle  
$$
$$
f_{\sigma \bar{\sigma}} (\gamma;\vec{r})  = 
\left \langle \Psi \left| \sum_{i=1}^{N} \delta(\vec{r}-\vec{r}_i)
\sum_{j ; \sigma_i \neq \sigma_j}^{N} 
e^{ -\gamma |\vec{r}_i - \vec{r}_j|^2 } \right| \Psi \right \rangle
$$
@end tex
If @math{\gamma} is not large enough in regions where
electrons are close to each other, the value of @math{f_{\sigma \sigma}} or
@math{f_{\sigma \bar{\sigma}}} can be greater than 1. In that case, the
approximation of the min function is irrelevant since its value becomes negative.
On the other hand, if @math{\gamma} is too large @math{f_{\sigma \sigma}} and
@math{f_{\sigma \bar{\sigma}}} have almost always extremely small values. In this
case, the values of @math{f_{\sigma \sigma}} and @math{f_{\sigma \bar{\sigma}}} are in practice not
accurately representable with 64 bit floating point numbers, as they may be
rounded to zero.  We propose @math{\gamma} to depend on the density, such that the
largest representable value of @math{d_{\sigma \sigma}} is equal to the radius of a
sphere that contains almost 0.1 electron:
@tex
$$
\gamma(\vec{r}) = \left({4\pi \over 3 n} \rho(\vec{r}) \right)^{2/3} (-\ln (\epsilon)) 
$$
@end tex
where @math{n=0.1} and @math{\epsilon} is the smallest floating point number representable with
64 bits (@math{\sim 2.10^{-308}}).



