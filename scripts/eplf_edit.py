#!/usr/bin/env python3

from math import *

import input_wrapper
InputFile = input_wrapper.InputFile
rw_data = list(input_wrapper.rw_data)
rw_data.sort()
ro_data = list(input_wrapper.rw_data)
ro_data.sort()

import sys, os
import tempfile

editor = os.getenv("EDITOR",default="vi")

## Data cleaning
##################

class Cleaner(object):
  def __init__(self,inp,name):
    self.dir = inp.name
    self.name = name.lower()
    self.inp = inp
  def action(self):
    raise TypeError
  def clean(self):
    print("Removing "+self.name)
    self.action()

class GridDataCleaner(Cleaner):
  def action(self):
    os.system("rm -f %s/grid_data/%s.gz"%(self.dir,self.name) )

class AllCleaner(Cleaner):
  def action(self):
    os.system("rm -rf %s/grid"%(self.dir) )
    os.system("rm -rf %s/grid_data"%(self.dir) )

def clear(x,inp):
  d = { "all": AllCleaner,
      }
  if x in list(d.keys()):
    type = d[x]
  else:
    type = GridDataCleaner

  type(inp,x.replace("_"," ")).clean()

## Data editing
###############

class Editor(object):
  def __init__(self,inp,name):
    self.dir = inp.name
    self.name = name
    self.inp = inp
  def action(self):
    raise TypeError
  def edit(self):
    print("Editing "+self.name)
    self.action()
    
class GeometryEditor(Editor):
  def action(self):
    self.charge = self.inp.nucl_charge
    self.coord = self.inp.nucl_coord
    edit_temp_file(self.inp,self.write_geom_file,self.read_geom_file)

  def write_geom_file(self,file,inp):
    print("###########################", file=file)
    print("# Geometry : %s"%(inp.name), file=file)
    print("###########################", file=file)
    print("", file=file)
    fields = "Charge X Y Z"
    format = "%10.1f "+"%10.6f "*3
    fields = fields.split()
    print(" ".join(["#"]+[x.center(10) for x in fields]), file=file)
    print("# "+"-"*11*len(fields)+"\n", file=file)
    charge = self.charge
    coord = self.coord
    for i in range(len(charge)):
      buffer = [ charge[i], coord[0][i], coord[1][i], coord[2][i] ]
      print("  "+format%tuple(buffer), file=file)

  def read_geom_file(self,file,inp):
    lines = file.readlines()
    lines = [x for x in lines if len(x.strip())>0]
    lines = [x for x in lines if not x.startswith("#")]
    lines = [x.split() for x in lines]
    coord = [ [], [], [] ]
    charge = []
    for line in lines:
      charge.append(float(line[-4]))
      coord[0].append(float(line[-3]))
      coord[1].append(float(line[-2]))
      coord[2].append(float(line[-1]))
    inp.coord = coord
      
class GridEditor(Editor):
  def action(self):
    self.origin= self.inp.origin
    self.opposite = self.inp.opposite
    self.step_size = self.inp.step_size
    self.point_num = self.inp.point_num
    edit_temp_file(self.inp,self.write_grid_file,self.read_grid_file)

  def write_grid_file(self,file,inp):
    print("###########################", file=file)
    print("# Grid : %s"%(inp.name), file=file)
    print("###########################", file=file)
    print("", file=file)
    print("# Number of points along x, y, z", file=file)
    if self.point_num is None:
      print("Default", file=file)
    else:
      print("%d %d %d"%tuple(self.point_num), file=file)
    print("", file=file)
    print("# Coordinates of the origin (x,y,z)", file=file)
    if self.origin is None:
      print("Default", file=file)
    else:
      print("%f %f %f"%tuple(self.origin), file=file)
    print("", file=file)
    print("# Coordinates of the opposite point (x,y,z)", file=file)
    if self.opposite is None:
      print("Default", file=file)
    else:
      print("%f %f %f"%tuple(self.opposite), file=file)
    print("", file=file)
    print("# Step sizes (x,y,z)", file=file)
    if self.step_size is None:
      print("Default", file=file)
    else:
      print("%f %f %f"%tuple(self.step_size), file=file)

  def read_grid_file(self,file,inp):
    lines = file.readlines()
    lines = [x for x in lines if len(x.strip())>0]
    lines = [x for x in lines if not x.startswith("#")]

    buffer = lines.pop(0).lower().split()
    if buffer[0] != "default":
      self.inp.point_num = list(map(int,buffer))
    else:
      try:
       os.remove("%s/grid/point_num.gz"%self.inp.name)
      except:
       pass

    buffer = lines.pop(0).lower().split()
    if buffer[0] != "default":
      self.inp.origin = list(map(float,buffer))
    else:
      try:
       os.remove("%s/grid/origin.gz"%self.inp.name)
      except:
       pass

    buffer = lines.pop(0).lower().split()
    if buffer[0] != "default":
      self.inp.opposite = list(map(float,buffer))
    else:
      try:
       os.remove("%s/grid/opposite.gz"%self.inp.name)
      except:
       pass

    buffer = lines.pop(0).lower().split()
    if buffer[0] != "default":
      self.inp.step_size = self.step_size
    else:
      try:
       os.remove("%s/grid/step_size.gz"%self.inp.name)
      except:
       pass

      

def edit(x,inp):
  d = { "geometry": GeometryEditor,
        "grid_parameters": GridEditor,
      }
  d[x](inp,x.replace("_"," ")).edit()

## Write temporary input file
#############################

def write_main_file(file,inp):
  print("####################", file=file)
  print("# %s"%(inp.name), file=file)
  print("####################", file=file)
  print("", file=file)
  compute = [x for x in rw_data if x.startswith("compute")]
  print("# Clear", file=file)
  print("# --------------------\n", file=file)
  print('# clear(all)', file=file)
  for p in compute:
    print('# clear(%s)'%(p[8:]), file=file)
  print("", file=file)
  print("# Computation", file=file)
  print("# --------------------\n", file=file)
  print("nproc = %d"%(inp.nproc), file=file)
  print("", file=file)
  for p in compute:
    x = ' '
    exec("if inp.%s: x = 'X'"%(p))
    print("(%s) %s"%(x,p[8:]), file=file)
  print("", file=file)
  print("# Edit", file=file)
  print("# --------------------\n", file=file)
  print('# edit(geometry)', file=file)
  print('# edit(grid_parameters)', file=file)
  print("", file=file)


## Execute temporary input file
###############################

def read_main_file(file,inp):

  lines = file.readlines()
  for line in lines:
    line = line.lstrip()
    if line == "" or line[0] == "#":
      pass
    elif line[0] == "(":
      line = line.replace("( ) ","=False inp.compute_")
      line = line.replace("(X) ","=True inp.compute_")
      line = line.replace("(x) ","=True inp.compute_")
      buffer = line.split()
      line = ' '.join([buffer[1].lower(),buffer[0]])
    elif line.startswith("edit") \
      or line.startswith("clear"):
      line = line.replace("(","('").replace(")","',inp)").lower()
    else:
      line = "inp."+line
    exec(line)


def edit_temp_file(input_file,write_file,read_file,saved_file=None):
  if saved_file is None:
    file,filename = tempfile.mkstemp()
    file = open(filename,"w")
    write_file(file,input_file)
    file.close()
    os.system("%s %s"%(editor,filename))
  else:
    file = open(saved_file,'r')
    buffer = file.read()
    file.close()

    file,filename = tempfile.mkstemp()
    file = open(filename,"w")
    file.write(buffer)    
    file.close()
  file = open(filename,"r")
  read_file(file,input_file)
  file.close()

  os.remove(filename)


def build_script(inp):
  run_script = "run_"+inp.name
# try:
#   file = open(run_script,'r')
#   file.close()
#   return
# except:
#   pass
  file = open(run_script,'w')
  buffer = """
  #!/bin/bash
source ${HOME}/.eplfrc
$RUNCOMMAND
""".lstrip()
  command = "${EPLF_PATH}/bin/eplf %s"%(inp.name)
  if inp.has_mpi:
    if inp.nproc <= 0:
      command = "${EPLF_MPIRUN} "+command
    else:
      command = "${EPLF_MPIRUN} -np %d "%(inp.nproc)+command
  buffer = buffer.replace("$RUNCOMMAND",command)
  print(buffer, file=file)
  file.close()
  os.chmod(run_script,0o744)


def main():
  if len(sys.argv) not in [2,3]:
    print("Syntax : %s <EZFIO_File> [input_file]"%(sys.argv[0]))
    sys.exit(1)
  inp = InputFile(sys.argv[1])
  if len(sys.argv) == 3:
    saved_file = sys.argv[2]
  else:
    saved_file = None
  edit_temp_file(inp,write_main_file,read_main_file,saved_file)
  build_script(inp)

main()
