#!/usr/bin/python

from ezfio import ezfio
import os

geom_data = """
nucl_charge nucl_coord
""".split()
ro_data = """det_num nucl_num mo_num ao_num has_mpi
""".split()
rw_data = """
point_num step_size origin opposite nproc
compute_elf compute_eplf compute_density
compute_elf_grad compute_eplf_grad compute_density_grad
compute_elf_lapl compute_eplf_lapl compute_density_lapl
compute_elf_partition compute_eplf_partition compute_density_partition
""".split()
rw_data_full = rw_data + geom_data

######################################################################
DEFAULT_COMPUTE_EPLF          = True
DEFAULT_COMPUTE_ELF           = False
DEFAULT_COMPUTE_DENSITY       = False
DEFAULT_COMPUTE_EPLF_GRAD     = False
DEFAULT_COMPUTE_ELF_GRAD      = False
DEFAULT_COMPUTE_DENSITY_GRAD  = False
DEFAULT_COMPUTE_EPLF_LAPL     = False
DEFAULT_COMPUTE_ELF_LAPL      = False
DEFAULT_COMPUTE_DENSITY_LAPL  = False
DEFAULT_COMPUTE_EPLF_PART     = False
DEFAULT_COMPUTE_ELF_PART      = False
DEFAULT_COMPUTE_DENSITY_PART  = False
DEFAULT_POINT_NUM             = [80,80,80]

######################################################################

class InputFileExn(Exception):
  """Errors in input file raise this exception"""

  def __init__(self,msg):
    self.msg = msg

  def __str__(self):
    result = "\n\n=========================================\n"
    result += self.msg
    result += "\n=========================================\n"
    return result


######################################################################
class InputFile(object):

  def __init__(self,name):
    assert isinstance(name,str)
    wd = os.getcwd()
    try:
      os.chdir(name)
    except OSError:
      raise InputFileExn("File not found")
    try:
      file = open(".version","r")
      file.close()
    except IOError:
      raise InputFileExn("This directory is not a EZFIO file.")
    os.chdir(wd)
    ezfio.set_filename(name)
    l = len(name)
    while name[l-1] == '/':
      l -= 1
    self.name = name[:l]
      
  # Read-only values
  def get_has_mpi(self):
    if os.getenv("EPLF_HAS_MPI","1") == "1":
      return True
    else:
      return False

  def get_det_num(self):
    if ezfio.has_determinants_det_num():
      return ezfio.get_determinants_det_num()
    else:
      return None

  def get_nucl_num(self):
    if ezfio.has_nuclei_nucl_num():
      return ezfio.get_nuclei_nucl_num()
    else:
      return None

  def get_ao_num(self):
    if ezfio.has_ao_basis_ao_num():
      return ezfio.get_ao_basis_ao_num()
    else:
      return None

  def get_mo_num(self):
    if ezfio.has_mo_basis_mo_tot_num():
      return ezfio.get_mo_basis_mo_tot_num()
    else:
      return None

  # Read/write values

  def get_nucl_coord(self):
    if ezfio.has_nuclei_nucl_coord():
      return ezfio.get_nuclei_nucl_coord()
    else:
      return None

  def set_nucl_coord(self,value):
    if not isinstance(value,list):
      raise InputFileExn("Wrong type")
    for i in value:
      try:
        float(i)
      except:
        raise InputFileExn("Wrong type")
    ezfio.set_nuclei_nucl_coord(value)

  def get_nucl_charge(self):
    if ezfio.has_nuclei_nucl_charge():
      return ezfio.get_nuclei_nucl_charge()
    else:
      return None

  def set_nucl_charge(self,value):
    if not isinstance(value,list):
      raise InputFileExn("Wrong type")
    for i in value:
      try:
        float(i)
      except:
        raise InputFileExn("Wrong type")
    ezfio.set_nuclei_nucl_charge(value)

  def get_point_num(self):
    if ezfio.has_grid_point_num():
      return ezfio.get_grid_point_num()
    else:
      return DEFAULT_POINT_NUM

  def set_point_num(self,value):
    if not isinstance(value,list):
      raise InputFileExn("Wrong type")
    if len(value) != 3:
      raise InputFileExn("Wrong type")
    for i in value:
      try:
        int(i)
      except:
        raise InputFileExn("Wrong type")
    ezfio.set_grid_point_num(value)

  def get_step_size(self):
    if ezfio.has_grid_step_size():
      return ezfio.get_grid_step_size()
    else:
      return None

  def set_step_size(self,value):
    if not isinstance(value,list):
      raise InputFileExn("Wrong type")
    for i in value:
      try:
        float(i)
      except:
        raise InputFileExn("Wrong type")
    ezfio.set_grid_step_size(value)


  def get_origin(self):
    if ezfio.has_grid_origin():
      return ezfio.get_grid_origin()
    else:
      return None

  def set_origin(self,value):
    if not isinstance(value,list):
      raise InputFileExn("Wrong type")
    if len(value) != 3:
      raise InputFileExn("Wrong type")
    for i in value:
      if not isinstance(i,float):
        raise InputFileExn("Wrong type")
    for j in value:
      try:
        float(j)
      except:
        raise InputFileExn("Wrong type")
    ezfio.set_grid_origin(value)


  def get_opposite(self):
    if ezfio.has_grid_opposite():
      return ezfio.get_grid_opposite()
    else:
      return None

  def set_opposite(self,value):
    if not isinstance(value,list):
      raise InputFileExn("Wrong type")
    if len(value) != 3:
      raise InputFileExn("Wrong type")
    for i in value:
      if not isinstance(i,float):
        raise InputFileExn("Wrong type")
    for j in value:
      try:
        float(j)
      except:
        raise InputFileExn("Wrong type")
    ezfio.set_grid_opposite(value)

  def get_nproc(self):
    if ezfio.has_compute_nproc():
      return ezfio.get_compute_nproc()
    else:
      return 1

  def set_nproc(self,value):
    if not isinstance(value,int):
      raise InputFileExn("Wrong type")
    if value < 1:
      raise InputFileExn("Wrong type")
    ezfio.set_compute_nproc(value)

  def get_compute_elf(self):
    if ezfio.has_compute_elf():
      return ezfio.get_compute_elf()
    else:
      return DEFAULT_COMPUTE_ELF

  def set_compute_elf(self,value):
    if not isinstance(value,bool):
      raise InputFileExn("Wrong type")
    ezfio.set_compute_elf(value)

  def get_compute_eplf(self):
    if ezfio.has_compute_eplf():
      return ezfio.get_compute_eplf()
    else:
      return DEFAULT_COMPUTE_EPLF

  def set_compute_eplf(self,value):
    if not isinstance(value,bool):
      raise InputFileExn("Wrong type")
    ezfio.set_compute_eplf(value)

  def get_compute_density(self):
    if ezfio.has_compute_density():
      return ezfio.get_compute_density()
    else:
      return DEFAULT_COMPUTE_DENSITY

  def set_compute_density(self,value):
    if not isinstance(value,bool):
      raise InputFileExn("Wrong type")
    ezfio.set_compute_density(value)


  def get_compute_elf_grad(self):
    if ezfio.has_compute_elf_grad():
      return ezfio.get_compute_elf_grad()
    else:
      return DEFAULT_COMPUTE_ELF_GRAD

  def set_compute_elf_grad(self,value):
    if not isinstance(value,bool):
      raise InputFileExn("Wrong type")
    ezfio.set_compute_elf_grad(value)

  def get_compute_eplf_grad(self):
    if ezfio.has_compute_eplf_grad():
      return ezfio.get_compute_eplf_grad()
    else:
      return DEFAULT_COMPUTE_EPLF_GRAD

  def set_compute_eplf_grad(self,value):
    if not isinstance(value,bool):
      raise InputFileExn("Wrong type")
    ezfio.set_compute_eplf_grad(value)

  def get_compute_density_grad(self):
    if ezfio.has_compute_density_grad():
      return ezfio.get_compute_density_grad()
    else:
      return DEFAULT_COMPUTE_DENSITY_GRAD

  def set_compute_density_grad(self,value):
    if not isinstance(value,bool):
      raise InputFileExn("Wrong type")
    ezfio.set_compute_density_grad(value)


  def get_compute_elf_lapl(self):
    if ezfio.has_compute_elf_lapl():
      return ezfio.get_compute_elf_lapl()
    else:
      return DEFAULT_COMPUTE_ELF_LAPL

  def set_compute_elf_lapl(self,value):
    if not isinstance(value,bool):
      raise InputFileExn("Wrong type")
    ezfio.set_compute_elf_lapl(value)

  def get_compute_eplf_lapl(self):
    if ezfio.has_compute_eplf_lapl():
      return ezfio.get_compute_eplf_lapl()
    else:
      return DEFAULT_COMPUTE_EPLF_LAPL

  def set_compute_eplf_lapl(self,value):
    if not isinstance(value,bool):
      raise InputFileExn("Wrong type")
    ezfio.set_compute_eplf_lapl(value)

  def get_compute_density_lapl(self):
    if ezfio.has_compute_density_lapl():
      return ezfio.get_compute_density_lapl()
    else:
      return DEFAULT_COMPUTE_DENSITY_LAPL

  def set_compute_density_lapl(self,value):
    if not isinstance(value,bool):
      raise InputFileExn("Wrong type")
    ezfio.set_compute_density_lapl(value)

  def get_compute_elf_partition(self):
    if ezfio.has_compute_elf_partition():
      return ezfio.get_compute_elf_partition()
    else:
      return DEFAULT_COMPUTE_ELF_PART

  def set_compute_elf_partition(self,value):
    if not isinstance(value,bool):
      raise InputFileExn("Wrong type")
    ezfio.set_compute_elf_partition(value)

  def get_compute_eplf_partition(self):
    if ezfio.has_compute_eplf_partition():
      return ezfio.get_compute_eplf_partition()
    else:
      return DEFAULT_COMPUTE_ELF_PART

  def set_compute_eplf_partition(self,value):
    if not isinstance(value,bool):
      raise InputFileExn("Wrong type")
    ezfio.set_compute_eplf_partition(value)

  def get_compute_density_partition(self):
    if ezfio.has_compute_density_partition():
      return ezfio.get_compute_density_partition()
    else:
      return DEFAULT_COMPUTE_DENSITY_PART

  def set_compute_density_partition(self,value):
    if not isinstance(value,bool):
      raise InputFileExn("Wrong type")
    ezfio.set_compute_density_partition(value)

  # Build the corresponding properties
  for i in ro_data:
    exec("%s = property(fget=get_%s,fset=None)"%(i,i))
    
  for i in rw_data_full:
    exec("%s = property(fget=get_%s,fset=set_%s)"%(i,i,i))



