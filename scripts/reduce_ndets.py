#!/usr/bin/env python

import common
import sys,os,time
from ezfio import ezfio
from math import *

# Check command line

firstArg = sys.argv[1]

if len(sys.argv) == 2:
   det_thr=0.001
elif len(sys.argv) == 3:
   det_thr=float(sys.argv[2])
else:
   print("usage: "+sys.argv[0]+" file.out det_threshold")
   sys.exit(2)

def reduce_dets():
  ezfio.set_file(firstArg)
  os.system ("cp -r %s/determinants %s/determinants.bak"%(firstArg,firstArg))
  print("Initial number of determinants: ",ezfio.determinants_det_num) 
  coef = ezfio.determinants_det_coef 
  norm = sum( [x*x for x in coef] )
  print("Norm of the initial wave function: ", sqrt(norm))
  dets_a, dets_b = ezfio.determinants_det_occ

  to_remove = []
  for i, c in enumerate(coef):
    if abs(c) < det_thr:
      to_remove.append(i)

  to_remove.sort()
  to_remove.reverse()
  for i in to_remove:
    coef.pop(i)
    dets_a.pop(i)
    dets_b.pop(i)
  print("New number of determinants: ", len(coef))
  norm = sum( [x*x for x in coef] )
  print("Norm of the new wave function: ", sqrt(norm))
  ezfio.determinants_det_num = len(coef)
  ezfio.determinants_det_coef = coef
  ezfio.determinants_det_occ = dets_a+dets_b

def main():
  reduce_dets()

if __name__ == '__main__':
  main()

