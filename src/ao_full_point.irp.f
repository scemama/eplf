BEGIN_PROVIDER [ real, ao_value_p, (ao_num) ]
 implicit none
 BEGIN_DOC
! Values of the atomic orbitals
 END_DOC
 integer :: i
 do i=1,ao_num
    ao_value_p(i) = ao_oneD_p(i) * ao_axis_p(i)
 enddo
END_PROVIDER

BEGIN_PROVIDER [ real, ao_grad_p, (ao_num,3) ]
 implicit none
 BEGIN_DOC
   ! Gradients of the atomic orbitals 
 END_DOC

  integer :: i,l
  do l=1,3
    do i=1,ao_num
     ao_grad_p(i,l) = ao_oneD_p(i) * ao_axis_grad_p(i,l) 
    enddo
    do i=1,ao_num
     ao_grad_p(i,l) = ao_grad_p(i,l) + ao_oneD_grad_p(i,l) * ao_axis_p(i) 
    enddo
  enddo

END_PROVIDER

BEGIN_PROVIDER [ real, ao_lapl_p, (ao_num) ]
 implicit none
 BEGIN_DOC
! Laplacian of the atomic orbitals 
 END_DOC
  integer :: i,l
  do i=1,ao_num
    ao_lapl_p(i) = ao_oneD_p(i) * ao_axis_lapl_p(i) 
  enddo
  do i=1,ao_num
     ao_lapl_p(i) = ao_lapl_p(i) + ao_oneD_lapl_p(i) * ao_axis_p(i) 
  enddo
  do l=1,3
    do i=1,ao_num
     ao_lapl_p(i) = ao_lapl_p(i) + 2.*ao_oneD_grad_p(i,l) * ao_axis_grad_p(i,l)
    enddo
  enddo

END_PROVIDER

