BEGIN_PROVIDER [ real, ao_oneD_prim_p, (ao_num,ao_prim_num_max) ]
 implicit none

 BEGIN_DOC
! Exponentials of the primitive AOs
 END_DOC
 integer :: i,  k
 real:: r2, rtemp

! Compute alpha*r or alpha*r^2
   do k=1,ao_prim_num_max
    do i=1,ao_num
      ao_oneD_prim_p(i,k) = point_nucl_dist_2(ao_nucl(i))
    enddo
   enddo

   ! Compute exp(-alpha*r) or exp(-alpha*r^2)
   do k=1,ao_prim_num_max
     do i=1,ao_num
       ao_oneD_prim_p(i,k) = exp(-ao_oneD_prim_p(i,k)*ao_expo_transp(i,k))
     enddo
   enddo

   ! Cut below 1.d-12
   do k=1,ao_prim_num_max
     do i=1,ao_num
       if ( abs(ao_oneD_prim_p(i,k)) < 1.e-12 ) then
         ao_oneD_prim_p(i,k) = 0.
       endif
     enddo
   enddo

END_PROVIDER

BEGIN_PROVIDER [ real, ao_oneD_p, (ao_num) ]
 implicit none

 BEGIN_DOC
! One-dimensional component of the AOs
 END_DOC

 integer :: i, k

 do i=1,ao_num
   ao_oneD_p(i) = 0.
 enddo
 do k=1,ao_prim_num_max
   do i=1,ao_num
     ao_oneD_p(i) = ao_oneD_p(i) + ao_coef_transp(i,k)*ao_oneD_prim_p(i,k)
   enddo
 enddo

END_PROVIDER


BEGIN_PROVIDER [ real, ao_oneD_prim_grad_p, (ao_num,ao_prim_num_max,3) ]
 implicit none

 BEGIN_DOC
! Gradients of the one-dimensional component of the primitive AOs
 END_DOC
 integer :: i, k, l
 real:: factor
 do l=1,3
   do k=1,ao_prim_num_max
    do i=1,ao_num
     factor = -2.*point_nucl_dist_vec(ao_nucl(i),l)
     ao_oneD_prim_grad_p(i,k,l) = factor*ao_expo_transp(i,k)*ao_oneD_prim_p(i,k)
    enddo
   enddo
 enddo

END_PROVIDER

BEGIN_PROVIDER [ real, ao_oneD_grad_p, (ao_num,3) ]
 implicit none

 BEGIN_DOC
! Gradients of the one-dimensional component of the AOs
 END_DOC
 integer :: i, k, l
 do l=1,3
   do i=1,ao_num
     ao_oneD_grad_p(i,l) = 0.
   enddo
   do k=1,ao_prim_num_max
    do i=1,ao_num
      ao_oneD_grad_p(i,l) = ao_oneD_grad_p(i,l) + ao_coef_transp(i,k)*ao_oneD_prim_grad_p(i,k,l)
    enddo
   enddo
 enddo

END_PROVIDER

BEGIN_PROVIDER [ real, ao_oneD_prim_lapl_p, (ao_num,ao_prim_num_max) ]
 implicit none

 BEGIN_DOC
! Laplacian of the one-dimensional component of the primitive AOs
 END_DOC
 integer :: i, k, l
 do k=1,ao_prim_num_max
   do i=1,ao_num
    ao_oneD_prim_lapl_p(i,k) = ao_oneD_prim_p(i,k) * ao_expo_transp(i,k) * &
       ( 4.*ao_expo_transp(i,k)*point_nucl_dist_2(ao_nucl(i)) - 6. )
   enddo
 enddo

END_PROVIDER

BEGIN_PROVIDER [ real, ao_oneD_lapl_p, (ao_num) ]
 implicit none

 BEGIN_DOC
! Laplacian of the one-dimensional component of the AOs
 END_DOC

 integer :: i, k

 do i=1,ao_num
   ao_oneD_lapl_p(i) = 0.
 enddo
 do k=1,ao_prim_num_max
   do i=1,ao_num
     ao_oneD_lapl_p(i) = ao_oneD_lapl_p(i) + ao_coef_transp(i,k)*ao_oneD_prim_lapl_p(i,k)
   enddo
 enddo

END_PROVIDER

