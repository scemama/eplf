BEGIN_TEMPLATE

BEGIN_PROVIDER [ logical, comp_$X ]
 implicit none
 BEGIN_DOC  
! If true, compute $X
 END_DOC
 comp_$X = .False.
 call get_compute_$X(comp_$X)
END_PROVIDER

SUBST [ X ]
 eplf;;
 eplf_grad;;
 eplf_lapl;;
 elf;;
 elf_grad;;
 elf_lapl;;
 density;;
 density_grad;;
 density_lapl;;
 elf_partition;;
 eplf_partition;;
 density_partition;;
END_TEMPLATE
