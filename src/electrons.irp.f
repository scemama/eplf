BEGIN_PROVIDER  [ integer, elec_alpha_num ]
 implicit none
 BEGIN_DOC
! Number of alpha electrons  
 END_DOC
 elec_alpha_num = -1
 call get_electrons_elec_alpha_num(elec_alpha_num)
 if (elec_alpha_num <= 0) then
   call abrt(irp_here,'Number of alpha electrons should be > 0')
 endif

END_PROVIDER

BEGIN_PROVIDER  [ integer, elec_beta_num ]
 implicit none
 BEGIN_DOC
! Number of beta electrons  
 END_DOC
 elec_beta_num = 0
 call get_electrons_elec_beta_num(elec_beta_num)
 if (elec_beta_num < 0) then
  call abrt(irp_here,'Number of beta electrons should be >= 0')
 endif

END_PROVIDER

BEGIN_PROVIDER [ integer, elec_num ]
 implicit none
 BEGIN_DOC
! Number of electrons
 END_DOC

 elec_num = elec_alpha_num + elec_beta_num
 ASSERT ( elec_num > 0 )

END_PROVIDER


BEGIN_PROVIDER [ integer, elec_num_2, (2) ]

 BEGIN_DOC
! Number of alpha and beta electrons in an array
 END_DOC

 elec_num_2(1) = elec_alpha_num
 elec_num_2(2) = elec_beta_num

END_PROVIDER

