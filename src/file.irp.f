BEGIN_PROVIDER  [ character*(64), ezfio_filename ]
 BEGIN_DOC
! Name of the ezfio file
 END_DOC
 IRP_IF MPI
  include 'mpif.h'
  integer :: ierr
 IRP_ENDIF

 if (mpi_master) then
   call getarg(1,ezfio_filename)
   if (ezfio_filename == '') then
     call ezfio_get_filename(ezfio_filename)
   endif
 endif

 IRP_IF MPI
  call MPI_BCAST(ezfio_filename,64,MPI_character,0,MPI_COMM_WORLD,ierr)
  if (ierr /= MPI_SUCCESS) then
    call abrt(irp_here,'Unable to broadcast ezfio_filename')
  endif
 IRP_ENDIF

 call ezfio_set_file(ezfio_filename)
 if (.not.mpi_master) then
   call ezfio_set_read_only(.True.)
 endif
 call barrier

END_PROVIDER

