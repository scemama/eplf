subroutine abrt (here,message)
  implicit none
  IRP_IF MPI
   include 'mpif.h'
  IRP_ENDIF
  character*(*) :: here
  character*(*) :: message
  print *, ''
  print *, '-------------------------'
  print *, 'Error in '//trim(here)//':'
  print *, '-------------------------'
  print *, trim(message)//'.'
  print *, '-------------------------'

  IRP_IF MPI
   integer :: ierr
   call mpi_abort(MPI_COMM_WORLD,0,ierr)
   ASSERT (ierr == MPI_SUCCESS)
  IRP_ELSE
   stop 1
  IRP_ENDIF
end

subroutine finish ()
  implicit none
  IRP_IF MPI
    include 'mpif.h'
    integer :: ierr
    call mpi_finalize(ierr)
    if (ierr /= MPI_SUCCESS) then
      call abrt(irp_here,"Unable to finalize MPI")
    endif
  IRP_ENDIF
end

