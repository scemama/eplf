BEGIN_TEMPLATE

 BEGIN_PROVIDER [ real, $X_grad_p, (3) ]
&BEGIN_PROVIDER [ real, $X_lapl_p ]
 implicit none
 BEGIN_DOC  
! Gradient and Laplacian of the EPLF at the current point.
 END_DOC

 real, parameter :: Delta=0.001
 integer :: l

 $X_lapl_p = -6.*$X_value_p

 do l=1,3
  point(l) = point(l)+Delta
  TOUCH point
  $X_grad_p(l) = $X_value_p
  $X_lapl_p    = $X_lapl_p + $X_value_p
  point(l) = point(l)-Delta-Delta
  TOUCH point
  $X_grad_p(l) = 0.5*($X_grad_p(l) - $X_value_p)/Delta
  $X_lapl_p    = $X_lapl_p + $X_value_p
  point(l) = point(l) + Delta
 enddo
 $X_lapl_p = $X_lapl_p/Delta**2
 TOUCH point

END_PROVIDER

SUBST [ X ]
 eplf;;
 elf;;

END_TEMPLATE
