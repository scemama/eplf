BEGIN_TEMPLATE

 subroutine $Xinfo (here,token,value)
  implicit none
  character*(*), intent(in)    :: here
  character*(*), intent(in)    :: token
  $Y, intent(in)         :: value
  if (mpi_master) then
   print *,  trim(here)//':'
   $Z
  endif
 end

SUBST [ X, Y, Z ]
  r; real;
   print *,  ' -> ', trim(token), '=', value;;
  d; double precision;
   print *,  ' -> ', trim(token), '=', value;;
  i; integer;
   print *,  ' -> ', trim(token), '=', value;;
  c; character*(*);
   print *,  ' -> ', trim(token), '=', value;;
  l; logical;
   if (value) then
     print *,  ' -> ', trim(token), '= True'
   else
     print *,  ' -> ', trim(token), '= False'
   endif;;
END_TEMPLATE

subroutine info(here,message)
  implicit none
  character*(*), intent(in) :: here, message
  if (mpi_master) then
     print *,  trim(here)//':'
     print *,  ' -> ', trim(message)
  endif
end

