program eplf
 implicit none
 provide mpi_rank

 if (comp_eplf) then
   PROVIDE grid_eplf
   FREE grid_eplf
 endif
 if (comp_eplf_grad.or.comp_eplf_lapl) then
   PROVIDE grid_eplf_grad
   PROVIDE grid_eplf_lapl
   FREE grid_eplf_grad
   FREE grid_eplf_lapl
 endif

 if (comp_elf) then
   PROVIDE grid_elf
   FREE grid_elf
 endif
 if (comp_elf_grad.or.comp_elf_lapl) then
   PROVIDE grid_elf_grad
   PROVIDE grid_elf_lapl
   FREE grid_elf_grad
   FREE grid_elf_lapl
 endif

 if (comp_density) then
   PROVIDE grid_density
   FREE grid_density
 endif
 if (comp_density_grad.or.comp_density_lapl) then
   PROVIDE grid_density_grad
   PROVIDE grid_density_lapl
   FREE grid_density_grad
   FREE grid_density_lapl
 endif

 if (comp_elf_partition) then
   PROVIDE grid_elf_partition
   FREE grid_elf_partition
 endif
 if (comp_eplf_partition) then
   PROVIDE grid_eplf_partition
   FREE grid_eplf_partition
 endif
 if (comp_density_partition) then
   PROVIDE grid_density_partition
   FREE grid_density_partition
 endif
 call finish()
end

