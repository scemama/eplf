BEGIN_PROVIDER [ integer, mo_closed_num ]
 implicit none
 BEGIN_DOC
! Number of closed shell Molecular orbitals
 END_DOC

 mo_closed_num = -1
 call get_mo_basis_mo_closed_num(mo_closed_num)
 if (mo_closed_num < 0) then
  call abrt(irp_here,'Number of closed-shell MOs should be >=0')
 endif
 if (mo_closed_num > elec_alpha_num) then
  call abrt(irp_here,'Number of closed-should MOs should be <= Number of alpha electrons')
 endif
 if (mo_closed_num > elec_beta_num) then
  call abrt(irp_here,'Number of closed-should MOs should be <= Number of beta electrons')
 endif

END_PROVIDER

BEGIN_PROVIDER [ integer, mo_active_num ]
 implicit none
 BEGIN_DOC
! Number of active Molecular orbitals
 END_DOC

 mo_active_num = -1
 call get_mo_basis_mo_active_num(mo_active_num)
 if (mo_active_num < 0) then
  call abrt(irp_here,'Number of active MOs should be >=0')
 endif

END_PROVIDER

BEGIN_PROVIDER [ integer, mo_num ]
 implicit none
 BEGIN_DOC
! Number of Molecular orbitals
 END_DOC

 mo_num = mo_closed_num + mo_active_num
 ASSERT(mo_num > 0)

END_PROVIDER

BEGIN_PROVIDER [ real, mo_coef, (ao_num,mo_num) ]
 implicit none
 BEGIN_DOC
! Molecular orbital coefficients
 END_DOC
 integer :: i, j
 real :: buffer(ao_num,mo_tot_num)

 buffer = 0.
 call get_mo_basis_mo_coef(buffer)
 do i=1,mo_num
  do j=1,ao_num
   mo_coef(j,i) = buffer(j,i)
  enddo
 enddo

END_PROVIDER

BEGIN_PROVIDER [ real, mo_coef_transp, (mo_num,ao_num) ]
 implicit none

 BEGIN_DOC
 ! Transposed array of mo coefficients
 END_DOC

 integer :: i, j, k

 do j=1,ao_num
  do k=1,mo_num
   mo_coef_transp(k,j) = mo_coef(j,k)
  enddo
 enddo

END_PROVIDER


BEGIN_PROVIDER [ integer, mo_coef_transp_non_zero_idx, (0:mo_num,ao_num) ]
 implicit none
 BEGIN_DOC
! Indices of the non-zero elements of the transpose of the Molecular orbital coefficients
 END_DOC
 integer :: i, j

 integer :: idx
 do j=1,ao_num
  idx = 0
  do i=1,mo_num
   if (mo_coef_transp(i,j) /= 0.) then
    idx += 1
    mo_coef_transp_non_zero_idx(idx,j) = i
   endif
  enddo
  mo_coef_transp_non_zero_idx(0,j) = idx
 enddo

END_PROVIDER

BEGIN_PROVIDER  [ logical, mo_is_closed, (mo_num) ]
&BEGIN_PROVIDER [ logical, mo_is_active, (mo_num) ]
  implicit none

  BEGIN_DOC
! mo_is_closed : True if mo(i) is a closed-shell
! mo_is_active : True if mo(i) is an active orbital
  END_DOC

  character :: buffer(mo_tot_num)
   
  integer :: i
  do i=1,mo_num
   if ( buffer(i) == 'c' ) then
     mo_is_closed(i) = .True. 
     mo_is_active(i) = .False.
   else if ( buffer(i) == 'a' ) then
     mo_is_closed(i) = .False. 
     mo_is_active(i) = .True.
   else
     mo_is_closed(i) = .False.
     mo_is_active(i) = .False.
   endif
  enddo
END_PROVIDER


BEGIN_PROVIDER [ integer, mo_tot_num ]

  BEGIN_DOC
! Total number of MOs in the EZFIO file
  END_DOC

  mo_tot_num = -1
  call get_mo_basis_mo_tot_num(mo_tot_num)
  if (mo_tot_num <= 0) then
   call abrt(irp_here,'Tota number of MOs can''t be <0')
  endif

END_PROVIDER


