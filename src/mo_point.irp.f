BEGIN_PROVIDER [ real, mo_value_p, (mo_num) ]
 implicit none

 BEGIN_DOC
! Values of the molecular orbitals
 END_DOC

 integer :: i, j, k

 do j=1,mo_num
   mo_value_p(j) = 0.
   do k=1,ao_num
    mo_value_p(j) = mo_value_p(j)+mo_coef(k,j)*ao_value_p(k)
   enddo
 enddo

END_PROVIDER

BEGIN_PROVIDER [ real, mo_value_prod_p, (mo_num,mo_num) ]
 implicit none

 BEGIN_DOC
! Products of 2 molecular orbitals
 END_DOC

 integer :: i, j, k

 do j=1,mo_num
   do k=1,mo_num
    mo_value_prod_p(k,j) = mo_value_p(k)*mo_value_p(j)
   enddo
 enddo

END_PROVIDER

BEGIN_PROVIDER [ real, mo_grad_p, (mo_num,3) ]
 implicit none

 BEGIN_DOC
! Gradients of the molecular orbitals
 END_DOC

 integer :: j, k, l

 do l=1,3
  do j=1,mo_num
    mo_grad_p(j,l) = 0.
    do k=1,ao_num
      mo_grad_p(j,l) = mo_grad_p(j,l) + mo_coef(k,j)*ao_grad_p(k,l)
    enddo
  enddo
 enddo
END_PROVIDER

BEGIN_PROVIDER [ real, mo_lapl_p, (mo_num) ]
 implicit none

 BEGIN_DOC
! Laplacians of the molecular orbitals
 END_DOC

 integer :: j, k

 do j=1,mo_num
   mo_lapl_p(j) = 0.
   do k=1,ao_num
     mo_lapl_p(j) = mo_lapl_p(j)+mo_coef(k,j)*ao_lapl_p(k)
   enddo
 enddo

END_PROVIDER

