BEGIN_PROVIDER [ logical, mpi_master ]
&BEGIN_PROVIDER [ integer, mpi_rank ]
&BEGIN_PROVIDER [ integer, mpi_size ]
 implicit none
 BEGIN_DOC  
! mpi_rank : Number of the processor

! mpi_size : Number of processors

! mpi_master : True if the current processor is the master
 END_DOC

 IRP_IF MPI
   include 'mpif.h'
   integer :: ierr
   call MPI_INIT(ierr)
   if (ierr /= MPI_SUCCESS) then
     call abrt(irp_here,"Unable to initialize MPI")
   endif
   call MPI_COMM_RANK(MPI_COMM_WORLD, mpi_rank, ierr)
   if (ierr /= MPI_SUCCESS) then
     call abrt(irp_here,"Unable to get MPI rank")
   endif
   call MPI_COMM_SIZE(MPI_COMM_WORLD, mpi_size, ierr)
   if (ierr /= MPI_SUCCESS) then
     call abrt(irp_here,"Unable to get MPI size")
   endif

 IRP_ELSE

   mpi_rank = 0
   mpi_size = 1

 IRP_ENDIF

 mpi_master = (mpi_rank == 0)

END_PROVIDER



subroutine barrier()
IRP_IF MPI
  include 'mpif.h'
  integer :: ierr
  call MPI_BARRIER(MPI_COMM_WORLD,ierr)
  if (ierr /= MPI_SUCCESS) then
    call abrt(irp_here,'Unable to realize MPI barrier')
  endif
IRP_ENDIF
end

