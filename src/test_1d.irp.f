program debug
 PROVIDE ao_prim_num_max
!read(*,*) eplf_gamma
!TOUCH eplf_gamma
 call run()
end

subroutine run
 implicit none
 point(1) = 0.
 point(2) = 0.
 integer :: i
 do i=-40,60
  point(3) = real(i)/20.
  TOUCH point
  print *,  point(3), eplf_value_p, eplf_up_up, eplf_up_dn
! print *,  point(3), density_beta_value_p
 enddo
end
