program to_cube
  implicit none

  integer :: i
  integer :: l
  integer :: ix, iy, iz
  integer :: iargc
  character*(128) :: filename
  character*(128) :: grid_type
  real, allocatable :: grid4D(:,:,:,:)
  real, allocatable :: grid3D(:,:,:)

  if (.not.mpi_master) then
   return
  endif
  if (iargc() /= 2) then
    print *,  'usage: to_cube filename grid_type'
    stop 1
  endif
  call getarg(2,grid_type)
  filename = trim(ezfio_filename)//'_'//trim(grid_type)//'.cube'
  open(unit=90,file=filename,status='UNKNOWN')
  write (90,*) 'Cube File'
  write (90,*) trim(ezfio_filename), ' ', trim(grid_type)
  write (90,10) nucl_num,(grid_origin(i), i=1,3)
  write (90,10) grid_x_num, grid_step(1), 0., 0.
  write (90,10) grid_y_num, 0., grid_step(2), 0.
  write (90,10) grid_z_num, 0., 0., grid_step(3)
  do i=1,nucl_num
   write (90,11) int(nucl_charge(i)), nucl_charge(i), &
   (nucl_coord(i,l),l=1,3)
  enddo
  BEGIN_TEMPLATE
  if (trim(grid_type) == '$X') then
   allocate (grid$DimD(grid_x_num,grid_y_num,grid_z_num $D))
   call get_grid_data_$X(grid$DimD)
   do ix = 1, grid_x_num
    do iy = 1, grid_y_num
     write (90,20) (grid$DimD(ix,iy,iz $D), iz=1, grid_z_num)
    enddo
   enddo
   deallocate(grid$DimD)
  endif
  SUBST [ X, Dim, D ]
   eplf; 3; ;;
   density;3; ;;
   density_lapl;3; ;;
   elf;3; ;;
   eplf_lapl;3; ;;
   elf_lapl;3; ;;
   elf_grad;4;,4;;
   density_grad;4;,4;;
   elf_grad;4;,4;;
   eplf_grad;4;,4;;
   elf_partition;3; ;;
   eplf_partition;3; ;;
   density_partition;3; ;;
  END_TEMPLATE
  10   format (2X,I3,3(2X,F10.6))
  11   format (2X,I3,4(2X,F10.6))
  20   format (6(E13.5))
  close(90)

end

