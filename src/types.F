      integer, parameter :: t_Gaussian = 1
      integer, parameter :: t_Slater   = 2

      character*(32)     :: types(t_Slater) = &
      (/ "Gaussian", "Slater", "Brownian", "Langevin", &
         "VMC", "DMC", "CI" /)

